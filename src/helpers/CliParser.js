import {Logger} from './Logger.js';
export class CliParser {

	// NOTE: Undefined values are allowed in the arguments!
	static parse(cliArguments) {
		let length = cliArguments.length;
		let parsedArguments = {};

		// Skip first argument (script name).
		for (let i = 2; i < length; i++) {
			// Read a key, assign its value.
			parsedArguments[cliArguments[i].slice(1)] = cliArguments[++i];
		}
		return parsedArguments;
	}
}
