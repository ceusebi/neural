export class Random {
  constructor() {}

	static get() {
		return Math.random();
	}

	static fromTo(from, to) {
		return Random.get() * (max - min) + min;
	}

	static intFromTo(from, to) {
		let min = Math.ceil(from);
		let max = Math.floor(to);
		return Math.floor(Random.get() * (max - min + 1) + min);
	}
}
