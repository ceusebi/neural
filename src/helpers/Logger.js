export class Logger {
  static initialize(debugLevel) {
    // NOTE:
    /*
      Debug level is transformed to an integer for easier comparision.
      0: "debug" Everything is logged. (debug, info, error, critical)
      1: "info" Logs marked as info or above are saved. (info, error, critical)
      2: "error" Only error and above are handled. (error, critical) Default.
      3: "critical" Critical level error logging only. (critical) Not recommended.
    */
    // Strings can't be used in a switch statement.
    if (debugLevel === undefined) {
      // No debugLevel was set. Use default debug level.
      Logger.level = 2;
      return;
    }
    if (debugLevel === "debug") {
      Logger.level = 0;
      return;
    }
    if (debugLevel === "info") {
      Logger.level = 1;
      return;
    }
    if (debugLevel === "error") {
      Logger.level = 2;
      return;
    }
    if (debugLevel === "critical") {
      Logger.level = 3;
      return;
    }
    // In case debugLevel didn't match any configuration.
    Logger.level = 2;
    return;
  }

  // Logs handlers.
  static debug(message) {
    if (Logger.level <= 0) {
      console.log(message);
    }
  }

  static info(message) {
    if (Logger.level <= 1) {
      console.log(message);
    }
  }

  static error(message) {
    if (Logger.level <= 2) {
      console.trace(message);
    }
  }

  static critical(message) {
    if (Logger.level <= 3) {
      console.trace(message);
    }
  }
}
