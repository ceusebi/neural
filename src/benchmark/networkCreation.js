import {Logger} from '../helpers/Logger.js';
import {CliParser} from '../helpers/CliParser.js';
import {Random} from '../helpers/Random.js';
import {NeuralNetwork} from '../network/NeuralNetwork.js'

let options = CliParser.parse(process.argv);
Logger.initialize(options.logLevel);
Logger.debug("Parsed Arguments: ")
Logger.debug(options);

function toSeconds (processTime) {
	return processTime[0] + processTime[1] / 1e9;
}

function toMilliseconds(processTime) {
	return toSeconds(processTime) * 1000;
}

let n =  options.n? parseInt(options.n) : 100;

Logger.info(`Creating ${n} random one layer configurations.`)

let oneLayerNetworkConfigurations = Array(n).fill(1).map(() => {
	return [
		Random.intFromTo(1, 10),
		Random.intFromTo(1, 100),
		Random.intFromTo(1, 10)
	];
});

let creationTimes = [];
let oneLayerNetworks = oneLayerNetworkConfigurations.map((config) => {
	let startMark = process.hrtime();
	let network =  new NeuralNetwork(config[0], config[1], config[2]);
	let stopTime = process.hrtime(startMark);
	creationTimes.push({stopTime, config});
});

let total = creationTimes.reduce((ac, elem) => {
	return ac + toMilliseconds(elem.stopTime);
}, 0);

creationTimes.sort((a, b) => {
	return toSeconds(a.stopTime) - toSeconds(b.stopTime);
});

let average = total / creationTimes.length;
let best = toMilliseconds(creationTimes[0].stopTime);
let worst = toMilliseconds(creationTimes[creationTimes.length -1].stopTime);

Logger.info(`Best took ${best} milliseconds.`);
Logger.info(`Worst took ${worst} milliseconds.`);
Logger.info(`Average took ${average} milliseconds.`);
Logger.info(`All took ${total} milliseconds.`);
