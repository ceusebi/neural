import {Logger} from './helpers/Logger.js';
import {CliParser} from './helpers/CliParser.js';
import {NeuralNetwork} from './network/NeuralNetwork.js'

let options = CliParser.parse(process.argv);
Logger.initialize(options.logLevel);
Logger.debug("Parsed Arguments: ")
Logger.debug(options);

let network = new NeuralNetwork(2,[3,3], 1);

let startMark = process.hrtime();
network.run([0.5, 0.7])
.then((result) => {
	let stopTime = process.hrtime(startMark);
	Logger.info(`Run took ${stopTime[0] + stopTime[1] / 1e9} seconds and resolved to ${result}`);
})
.catch(error => Logger.error(error));
