import {Logger} from '../../helpers/Logger.js';
import {Neuron} from './Neuron.js';
import {Synapsis} from './Synapsis.js';
import {Identity} from '../activationFunctions/Identity.js'


export class InputNeuron extends Neuron {
  constructor() {
    super([]);
  }

  setup() {
    // Override activation function since input neurons don't use one.
    this.activationFunction = Identity.of;
    // Override filled to resolve when data is set from the outside.
    this.filled = new Promise((resolve, reject) => {
      this.resolveFill = resolve;
    });
    return this;
  }

	// Data has some input data.
  setData(data) {
    this.data = data;
    Logger.debug(`${this.id} resolved ${this.data}`);
		this.resolveFill(this.data);
  }
}
