import {Logger} from '../../helpers/Logger.js';
import {Neuron} from './Neuron.js';
import {Synapsis} from './Synapsis.js';

export class OutputNeuron extends Neuron {
  constructor(synapses) {
    super(synapses);
  }

  // Override activate since it doen't have a synapsis to weight.
  activate(data) {
    return this.activationFunction(data);
  }

}
