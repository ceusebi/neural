import {Logger} from '../../helpers/Logger.js';
import {Sigmoid} from '../activationFunctions/Sigmoid.js'
import {Synapsis} from './Synapsis.js';

export class Neuron {
  constructor(synapses) {
    this.id = Neuron.getId();
    this.synapses = synapses.map((synapsis) => {
			synapsis.dst = this;
			return synapsis;
		})
  }

  setup() {
    this.activationFunction = Sigmoid.of;
    this.filled = new Promise((resolve, reject) => {
      Promise.all(this.synapses.map((synapsis) => {
        return synapsis.getData();
      }))
      .then((data) => {
        resolve(this.setData(data));
      })
      .catch((error) => {
        Logger.error(`${this.id} failed in the activation: ${error}`);
      });
    });
    return this;
  }

  activate(data, synapsis) {
    return this.activationFunction(data) * synapsis.weight;
  }

	// Data has all the results from it's predecesors.
  setData(data) {
    this.data = data.reduce((ac, elem) => {
      return ac + elem;
    }, 0);
    Logger.debug(`${this.id} resolved ${this.data}`);
		return this.data;
  }

	getData(synapsis) {
		return this.filled.then((data) => {
			return this.activate(data, synapsis);
		});
	}

  static id = function*() {
   let i = 0;
   while (true) {
     yield i++;
   }
 }();

 static getId() {
   return Neuron.id.next().value;
 }

}
