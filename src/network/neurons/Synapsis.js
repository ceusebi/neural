import {Logger} from '../../helpers/Logger.js';

export class Synapsis {
  constructor(src, dst, weight) {
    this.src = src;
    this.dst = dst;
    this.weight = weight;
  }

	getData() {
		return this.src.getData(this);
	}
	
}
