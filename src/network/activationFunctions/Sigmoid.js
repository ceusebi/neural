export class Sigmoid {

  static of(x) {
    // HACK: Sigmoid(x) = 1 / (1 + e^-x)
    return (1 / (1 + (Math.E ** -x)));
  }

  static dx(x) {
    // HACK: Sigmoid'(x) = Sigmoid(x) * (1 - Sigmoid(x))
    let sig = Sigmoid.of(x);
    return (sig - sig**2);
  }
}
