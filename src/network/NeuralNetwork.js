import {Logger} from '../helpers/Logger.js';
import {Neuron} from './neurons/Neuron.js';
import {InputNeuron} from './neurons/InputNeuron.js';
import {OutputNeuron} from './neurons/OutputNeuron.js';
import {Synapsis} from './neurons/Synapsis.js';

export class NeuralNetwork {
  constructor(inputNumber, layers,outputNumber) {
    // Create an array of inputNumber lenght, and make a new InputNeuron for each of them.
    this.inputs = Array(inputNumber).fill(1).map(() => {
      return new InputNeuron().setup();
    });

		let previousLayer = this.inputs;
		let synapses = previousLayer.map((neuron) => {
			return new Synapsis(neuron, undefined, Math.random());
		});

		for (var i = 0; i < layers.length; i++) {
			let currentLayer = Array(layers[i]).fill(1).map(() => {
				return new Neuron(synapses).setup();
			})
			previousLayer = currentLayer;
			synapses = previousLayer.map((neuron) => {
				return new Synapsis(neuron, undefined, Math.random());
			});
		}

    this.outputs = Array(outputNumber).fill(1).map(() => {
      return new OutputNeuron(synapses).setup();
    });

    Logger.debug(this.inputs);
    Logger.debug(this.outputs);
  }

  run(inputs) {
    Logger.debug("Inject input data");
    for (var i = 0; i < inputs.length; i++) {
      this.inputs[i].setData(inputs[i]);
    }
		Logger.debug("Ask for resolution")
		return Promise.all(this.outputs.map((neuron) => {
      // This are output neurons, so we dont pass a synapsis.
      return neuron.getData();
    }));
  }
}
